var config = require('./')

module.exports = {
  src: [
    config.sourceAssets + '/configs/**/*.json',
  ],
  dest: config.publicAssets + '/configs'
}
