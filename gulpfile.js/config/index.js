var config = {}

config.publicDirectory = "./public";
config.sourceDirectory = "./app";
config.node_modules = "./node_modules";
config.publicAssets    = config.publicDirectory + "/assets";
config.sourceAssets    = config.sourceDirectory + "/assets";

module.exports = config;
