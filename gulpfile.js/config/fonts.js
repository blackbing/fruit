var config = require('./')

module.exports = {
  src: [
    config.sourceAssets + '/fonts/**/*',
    config.node_modules + '/bootstrap/fonts/**/*'
  ],
  dest: config.publicAssets + '/fonts'
}
