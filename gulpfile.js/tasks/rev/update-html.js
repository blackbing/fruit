var gulp         = require('gulp');
var config       = require('../../config');
var revReplace = require('gulp-rev-replace')
var gitVersion = require('gulp-gitversion');
var minifyHTML = require('gulp-minify-html');


// 5) Update asset references in HTML
gulp.task('update-html', ['rev-css'], function(){
  var manifest = gulp.src(config.publicDirectory + "/rev-manifest.json");
  return gulp.src(config.publicDirectory + '/**/*.html')
    .pipe(revReplace({manifest: manifest}))
    .pipe( minifyHTML({

    }))
    .pipe(gitVersion({type:'html'}))
    .pipe(gulp.dest(config.publicDirectory));
});
