var browserSync = require('browser-sync');
var changed     = require('gulp-changed');
var config      = require('../config/style');
var autoprefixer = require('gulp-autoprefixer');
var gulp        = require('gulp');

gulp.task('style', function() {
  return gulp.src(config.src)
    .pipe(changed(config.dest)) // Ignore unchanged files
    .pipe(autoprefixer(config.autoprefixer))
    .pipe(gulp.dest(config.dest))
    .pipe(browserSync.reload({stream:true}));
});
