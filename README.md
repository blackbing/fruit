#Demo

<http://demo.blackbing.net/fruit>

# Screenshot
![Fruit](https://dl.dropboxusercontent.com/u/751847/Lab/Fruit.jpg)


# Main Component

* <https://github.com/codesuki/react-d3-components>
* <https://github.com/JedWatson/react-select>
* <https://github.com/react-bootstrap/react-bootstrap>

# Note

* I set proxy to query real data.
* Y is 平均價格.
* For convenient. the market is fixed to 市場名稱: "台北一"
* The fruit list selection is limited currently. I just pick up some keyword from the source data.
* After the "GO" button triggered, it will query different data at the same time, then display the data.

