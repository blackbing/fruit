let React = require('react');
let Select = require('react-select');

module.exports = React.createClass({

  getInitialState() {
    return {
      options: [
        { value: '椰子', label: '椰子' },
        { value: '小番茄-聖女', label: '小番茄-聖女' },
        { value: '櫻桃-進口', label: '櫻桃-進口' },
        { value: '榴槤-進口', label: '榴槤-進口' },
        { value: '鳳梨-金鑽鳳梨', label: '鳳梨-金鑽鳳梨' },
        { value: '冬瓜-白皮', label: '冬瓜-白皮' },
        { value: '苦瓜-翠綠', label: '苦瓜-翠綠' },
        { value: '釋迦', label: '釋迦' }
      ],
      selected: [{
        value: '椰子', label: '椰子'
      }]
    };
  },

  componentDidMount() {
  },

  onChange(val, selected) {
    console.log(selected);
    this.setState({
      selected: selected
    });
  },

  render(){
    return (
      <Select
        name="fruit"
        multi={true}
        value={this.state.selected}
        options={this.state.options}
        clearable={false}
        delimiter={","}
        onChange={this.onChange}
      />
    )
  }
});
