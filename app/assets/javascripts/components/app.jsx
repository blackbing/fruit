let React         = require('react');
let ReactD3 = require('react-d3-components');
let LineChart = ReactD3.LineChart;
let Brush = ReactD3.Brush;
let FruitSelection = require('./fruit-selection.jsx');
let DatePicker = require('./date-picker.jsx');
let ReactBootstrap = require('react-bootstrap');
let {
  Button
} = ReactBootstrap;
let SyncLoader = require('halogen/SyncLoader');

let API = require('../service/api');
let moment = require('moment');

var labelAccessor = function(stack) { return stack.customLabel; };
var tooltipLine = function(label, data) {
  let str = label + " x: " + moment(data.x).format('YYYY/MM/DD') + " y: " + data.y;
  return str;
};
module.exports = React.createClass({
  getInitialState() {
    return {
      isLoading: false,
      data: [],
      xScale: null //d3.time.scale().domain([new Date(2015, 2, 5), new Date(2015, 2, 26)]).range([0, 800 - 70])
    };
  },

  getFruit(fruit, start, end) {

    API.getFruit( fruit, start, end ).then( (res)=>{


      let data = this.state.data;
      let xScale = d3.time.scale().domain([start.toDate(), end.toDate()]).range([0, 800 - 70]);
      data.push(res);

      this.setState({
        isLoading: false,
        data: data,
        xScale: xScale
      });
    }, (res)=>{
      console.error(res);
      this.setState({isLoading: false});
    });
  },

  handleClick() {
    let dateRange = this.refs.date.state.dateRange;
    let fruits = this.refs.fruit.state.selected;
    if(dateRange && fruits) {
      this.setState({
        isLoading: true,
        data: []
      });
      console.log(fruits);
      _.map( fruits, (fruit)=>{
        this.getFruit( fruit.value, dateRange.start, dateRange.end );
      });
    }
  },

  renderChart() {
    if(this.state.isLoading) {
      return (
        <SyncLoader className="spinner" color="#4DAF7C"/>
      )
    }else{
      if(this.state.data.length) {
        return (
          <LineChart
            data={this.state.data}
            width={800}
            height={400}
            margin={{top: 10, bottom: 50, left: 50, right: 20}}
            xScale={this.state.xScale}
            xAxis={{tickValues: this.state.xScale.ticks(d3.time.day, 2), tickFormat: d3.time.format("%m/%d"), label:'date'}}
            yAxis={{label: "price"}}
            tooltipHtml={tooltipLine}
          />
        )
      }else{
        return null;
      }
    }
  },

  render() {
    return (
      <div>
        <div className="panel panel-default">
          <div className="panel-heading">Fruit Data Query</div>
          <div className="panel-body">
            <form className="form-inline">
              <div className="form-group">
                <DatePicker ref="date"/>
              </div>
              <div className="form-group fruit-selection">
                <FruitSelection ref="fruit"/>
              </div>
              <div className="form-group">
                <Button
                  bsStyle='success'
                  disabled={this.state.isLoading}
                  onClick={!this.state.isLoading ? this.handleClick : null}>
                  {this.state.isLoading ? 'Loading...' : 'GO'}
                </Button>
              </div>
            </form>
          </div>
        </div>
        <div>
          {this.renderChart()}
        </div>
      </div>
    )
  }
});

