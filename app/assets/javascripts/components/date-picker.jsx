
let React = require('react');
let ReactBootstrap = require('react-bootstrap');
let {
  Button,
  OverlayTrigger,
  Popover
} = ReactBootstrap;

let DateRangePicker = require('react-daterange-picker');
let moment = require('moment');

let stateDefinitions = {
  available: {
    color: null,
    label: 'Available'
  },
  enquire: {
    color: '#ffd200',
    label: 'Enquire'
  },
  unavailable: {
    selectable: false,
    color: '#78818b',
    label: 'Unavailable'
  }
};

const fmt = 'YYYY/MM/DD';
const initialStart = moment().subtract(2, 'weeks').startOf('day');
const initialEnd = moment().startOf('day');

module.exports = React.createClass({
  getInitialState() {
    return {
      dateRange: moment.range(initialStart, initialEnd)
    };
  },
  componentDidMount() {
  },
  handleSelect(momentObject, states) {
    this.setState({
      dateRange: momentObject
    });
  },
  componentWillUnmount() {
  },

  render() {

    let dateText = 'Date Range';
    if(this.state.dateRange) {
      dateText = [
        this.state.dateRange.start.format(fmt),
        this.state.dateRange.end.format(fmt)
      ].join(' ~ ');
    }

    let overlay = (
      <Popover >
        <DateRangePicker
          firstOfWeek={0}
          numberOfCalendars={2}
          selectionType='range'
          earliestDate={new Date()}
          stateDefinitions={stateDefinitions}
          defaultState="available"
          showLegend={false}
          value={this.state.dateRange}
          onSelect={this.handleSelect} />
      </Popover>
    );
    return (
      <OverlayTrigger rootClose={true} trigger="click" placement="bottom" overlay={overlay}>
        <Button className="date-selector" bsStyle='primary'>{dateText}</Button>
      </OverlayTrigger>
    )
  }

});

