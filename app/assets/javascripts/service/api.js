let xhttp = require('xhttp');
let cookie = require('cookie');
let _ = require('lodash');

const host = '/coa/';
let getDateString = function(momentObj){
  console.log(momentObj);
  return (
    (momentObj.year()-1911) + '.' +
    _.padLeft(momentObj.month()+1, 2, '0') + '.' +
    _.padLeft(momentObj.date(), 2, '0')
  );
};
class API {
  constructor() {
    //console.log(host);
  }
  getFruit(crop, startMoment, endMoment) {
    let encodeCrop = encodeURIComponent(crop);
    let start = getDateString(startMoment);
    let end = getDateString(endMoment);

    console.log(crop);
    console.log(start);
    console.log(end);

    return xhttp({
      url: `/coa/OpenData/FarmTransData.aspx?$top=1000&$skip=0&StartDate=${start}&EndDate=${end}&Crop=${encodeCrop}&Market=台北一`,
      method: 'get',
      type: 'json'
    }).then(function(data){
      data = JSON.parse(data);
      console.log('original data', data);
      let exportData = _(data).filter( (val)=>{
        return val['作物名稱'] === crop;
      }).map( (val)=>{
        let d = val['交易日期'].split('.');
        let d_y = parseInt(d[0], 10)+1911;
        let d_m = parseInt(d[1], 10)-1;
        let d_d = parseInt(d[2], 10);
        return {
          x:  new Date( d_y, d_m, d_d),
          y:  +val['平均價']
        }
      }).valueOf();


      return {
        label: crop,
        values: exportData
      };
    });
  }
}
module.exports = new API();
